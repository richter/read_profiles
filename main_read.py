import datetime
import sys
sys.path.append('./scripts/')
import readProfile
import process_profile

#%%
filename = './input/WFJ2.pro'
##########################################
########### Read *.pro-file ##############
##########################################
prof = readProfile.read_profile(filename,remove_soil=True)
####################################################################
########### Process profile ########################################
###### Compute critical cut length [cm] in the flat field ##########
###### Compute penetrations depth [m] ##############################
####################################################################
prof = process_profile.process_profile(prof)

####################################################################
############## Print header ########################################
####################################################################
print(prof['info'])



####################################################################
###########  Print snow depth for each timestamp ################
####################################################################
print('Loaded profile for Dates: ')
ts = sorted(prof['data'].keys())

for dt in ts:
    try: 
        print('snow depth on ' + datetime.datetime.strftime(dt, '%d.%m.%Y') + ': ' + str(prof['data'][dt]['height'][-1]) + ' cm ')
        break
    except: continue


####################################################################
########### Print layer properties which were loaded ##############
####################################################################
dt=datetime.datetime(2022,2,1,12)
pro = prof['data'][dt]

print(pro.keys())


####################################################################
###### Print critical cut length for each layer on 01.02.2022 ######
####################################################################
print(dt)
print('Critical cut length')
print(pro['critical_cut_length'])
